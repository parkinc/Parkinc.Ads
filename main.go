package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gopkg.in/olivere/elastic.v5"
	"gopkg.in/sohlich/elogrus.v2"
)

var log = logrus.New()
var redisClient *redis.Client

type ImageValidator struct {
}

func (iv *ImageValidator) IsValid(img *Image) bool {
	_, err := base64.StdEncoding.DecodeString(img.ImageData)
	return err == nil
}

type Image struct {
	ImageData string `json:"ImageData"`
}

func cacheImage(img Image) {
	log.Info("Saving to cache...")
	imgJson, err := json.Marshal(img)
	if err != nil {
		log.Error("Error marshaling image into JSON when caching")
	}
	err = redisClient.Set("ad", imgJson, 0).Err()
	if err != nil {
		log.Error(err)
	}
}
func retrieveImage() Image {
	log.Info("Retrieving from cache...")
	imgJson, err := redisClient.Get("ad").Result()
	if err != nil {
		log.Error("Error retrieving image from redis")
		log.Error(err)
	}

	var img Image
	err = json.Unmarshal([]byte(imgJson), &img)
	if err != nil {
		log.Error("Error unmarshalling image retrieved from redis")
	}
	return img
}

func getAd() (Image, error) {
	client := http.Client{
		Timeout: time.Duration(1 * time.Second),
	}
	resp, err := client.Get("http://dm.sof60.dk:81/api/ad")

	var image Image
	if err != nil {
		log.Info("Request to provider failed or timed out.")
		image = retrieveImage()
		return image, nil
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Image{}, err
	}

	err = json.Unmarshal(body, &image)
	if err != nil {
		return Image{}, err
	}

	validator := &ImageValidator{}
	if validator.IsValid(&image) {
		log.Info("Caching image")
		cacheImage(image)
	} else {
		log.Info("Image not valid, retrieving from cache...")
		image = retrieveImage()
	}

	return image, nil
}

func handleGetAd(w http.ResponseWriter, r *http.Request) {
	img, err := getAd()
	if err != nil {
		log.Error(err)
	}
	fmt.Fprint(w, img.ImageData)
	log.Info("Responded to /ad")
}

func main() {

	redisUrl := os.Getenv("REDIS_URL")

	logrus.Info("Starting Redis client at ", redisUrl)
	redisClient = redis.NewClient(&redis.Options{
		Addr:     redisUrl,
		Password: "",
		DB:       0,
	})

	pong, err := redisClient.Ping().Result()
	fmt.Println(pong, err)

	elasticClient, err := elastic.NewSimpleClient(elastic.SetURL("https://search-parkinc-pnvs65d6ggiqal6cjt7amv5wzu.eu-west-1.es.amazonaws.com"))
	if err != nil {
		panic(err)
	}
	hook, err := elogrus.NewElasticHook(elasticClient, "", logrus.InfoLevel, "logstash-2018.01.10")
	if err != nil {
		panic(err)
	}
	log.Hooks.Add(hook)

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/ad", handleGetAd)
	log.Fatal(http.ListenAndServe(":8080", router))
	log.Info("Server running at localhost:8080")
}
