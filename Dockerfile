FROM iron/go
ENV REDIS_URL="redis-master:6379"
COPY app /
EXPOSE 8080/tcp
ENTRYPOINT ["/app"]
